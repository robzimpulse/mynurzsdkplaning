//
//  ProfileFreelancer.swift
//  Pods
//
//  Created by Robyarta on 4/28/17.
//
//

import Foundation
import RealmSwift

public class ProfileFreelancer: Object{
    dynamic public var nameid = ""
    dynamic public var fullName = ""
    dynamic public var aboutMe = ""
    dynamic public var gender = ""
    dynamic public var dob = ""
    dynamic public var religion = ""
    dynamic public var address = ""
    dynamic public var addressDistrict = ""
    dynamic public var addressCity = ""
    dynamic public var addressZip = ""
    dynamic public var addressState = ""
    dynamic public var addressCountry = ""
    dynamic public var email = ""
    dynamic public var mobilePhone = ""
    dynamic public var phone = ""
    dynamic public var idCardUrl = ""
}

public class ProfileFreelancerController {
    
    public static let shared = ProfileFreelancerController()
    private var realm: Realm?
    
    public func get() -> ProfileFreelancer? {
        self.realm = try! Realm()
        return self.realm!.objects(ProfileFreelancer.self).first
    }
    
    func put(nameId: String, fullName: String, aboutMe: String, gender: String, dob: String, religion: String, address: String, addressDistrict: String, addressCity: String, addressZip: String, addressState: String, addressCountry: String, email: String, mobilePhone: String, phone: String, idCardUrl: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            let currentProfile = ProfileFreelancer()
            
            currentProfile.nameid = nameId
            currentProfile.fullName = fullName
            currentProfile.aboutMe = aboutMe
            currentProfile.gender = gender
            currentProfile.dob = dob
            currentProfile.religion = religion
            currentProfile.address = address
            currentProfile.addressDistrict = addressDistrict
            currentProfile.addressCity = addressCity
            currentProfile.addressZip = addressZip
            currentProfile.addressState = addressState
            currentProfile.addressCountry = addressCountry
            currentProfile.email = email
            currentProfile.mobilePhone = mobilePhone
            currentProfile.phone = phone
            currentProfile.idCardUrl = idCardUrl
            
            if let oldProfile = self.realm!.objects(ProfileFreelancer.self).first {
                self.realm?.delete(oldProfile)
            }
            self.realm!.add(currentProfile)
        }
    }
    
}
