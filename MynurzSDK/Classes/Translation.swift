//
//  Translation.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation
import RealmSwift

public class Translation: Object{
    dynamic public var id = ""
    dynamic public var name = ""
    
    override static public func primaryKey() -> String? {
        return "id"
    }
}

public class TranslationController {
    
    public static let shared = TranslationController()
    private var realm: Realm?
    
    func put(id: String, name: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(Translation.self, value: ["id":id, "name":name], update: true)
        }
    }
    
    public func getBy(id: String) -> Translation?{
        self.realm = try! Realm()
        return self.realm!.objects(Translation.self).filter("id = '\(id)'").first
    }
    
    
    public func getAll() -> Results<Translation> {
        self.realm = try! Realm()
        return self.realm!.objects(Translation.self)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(Translation.self))!)
        }
    }
    
}
