//
//  Language.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation
import RealmSwift

public class Language: Object{
    dynamic public var id = ""
    dynamic public var name = ""
    
    override static public func primaryKey() -> String? {
        return "id"
    }
}

public class LanguageController {
    
    public static let shared = LanguageController()
    private var realm: Realm?
    
    func put(id: String, name: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(Language.self, value: ["id":id, "name":name], update: true)
        }
    }
    
    public func getBy(name: String) -> Language {
        self.realm = try! Realm()
        if let language = self.realm!.objects(Language.self).filter("name = '\(name)'").first {
            return language
        }
        return Language()
    }
    
    public func getBy(id: String) -> Language{
        self.realm = try! Realm()
        if let language = self.realm!.objects(Language.self).filter("id = '\(id)'").first {
            return language
        }
        return Language()
    }
    
    public func getAll() -> Results<Language> {
        self.realm = try! Realm()
        return self.realm!.objects(Language.self)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(Language.self))!)
        }
    }
    
}
