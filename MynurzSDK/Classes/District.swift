//
//  District.swift
//  Pods
//
//  Created by Robyarta on 4/28/17.
//
//

import Foundation
import RealmSwift

public class District: Object {
    dynamic public var id = ""
    dynamic public var cityId = ""
    dynamic public var name = ""
    
    override static public func primaryKey() -> String? {
        return "id"
    }
    
}

public class DistrictController {
    
    public static let shared = DistrictController()
    private var realm: Realm?
    
    func put(id: String, cityId:String, name: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(District.self, value: ["id":id, "cityId": cityId, "name":name], update: true)
        }
    }
    
    public func getBy(id: String) -> District{
        self.realm = try! Realm()
        if let city = self.realm!.objects(District.self).filter("id = '\(id)'").first {
            return city
        }
        return District()
    }
    
    public func getBy(name: String) -> District{
        self.realm = try! Realm()
        if let city = self.realm!.objects(District.self).filter("name = '\(name)'").first {
            return city
        }
        return District()
    }
    
    public func getAll() -> Results<District> {
        self.realm = try! Realm()
        return self.realm!.objects(District.self)
    }
    
    public func getAllBy(cityId: String) -> Results<District> {
        self.realm = try! Realm()
        return self.realm!.objects(District.self).filter("cityId = '\(cityId)'")
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(District.self))!)
        }
    }
    
    
}
