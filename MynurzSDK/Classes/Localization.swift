//
//  Localization.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation
import RealmSwift

public class Localization: Object{
    dynamic public var region = ""
    dynamic public var language = ""
}

public class LocalizationController {
    
    public static let shared = LocalizationController()
    private var realm: Realm?
    
    public func get() -> Localization? {
        self.realm = try! Realm()
        return self.realm!.objects(Localization.self).first
    }
    
    func put(region: String, language: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            let currentLocalization = Localization()
            currentLocalization.region = region
            currentLocalization.language = language
            if let oldLocalization = self.realm!.objects(Localization.self).first {
                self.realm?.delete(oldLocalization)
            }
            self.realm!.add(currentLocalization)
        }
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(Localization.self))!)
        }
    }
    
}
