//
//  Service.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation
import RealmSwift

public class Service: Object {
    dynamic public var id = ""
    dynamic public var name = ""
    override static public func primaryKey() -> String? {
        return "id"
    }
}

public class ServiceController {
    
    public static let shared = ServiceController()
    private var realm: Realm?
    
    func put(id: String, name: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(Service.self, value: ["id":id, "name":name], update: true)
        }
    }
    
    public func getBy(id: String) -> Service{
        self.realm = try! Realm()
        if let location = self.realm!.objects(Service.self).filter("id = '\(id)'").first {
            return location
        }
        return Service()
    }
    
    public func getBy(name: String) -> Service{
        self.realm = try! Realm()
        if let location = self.realm!.objects(Service.self).filter("name = '\(name)'").first {
            return location
        }
        return Service()
    }
    
    public func getAll() -> Results<Service> {
        self.realm = try! Realm()
        return self.realm!.objects(Service.self)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(Service.self))!)
        }
    }
    
    
}
