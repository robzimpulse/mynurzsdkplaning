//
//  Location.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation
import RealmSwift

public class Location: Object {
    dynamic public var id = ""
    dynamic public var name = ""
    override static public func primaryKey() -> String? {
        return "id"
    }
}

public class LocationController {
    
    public static let shared = LocationController()
    private var realm: Realm?
    
    func put(id: String, name: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(Location.self, value: ["id":id, "name":name], update: true)
        }
    }
    
    public func getBy(id: String) -> Location{
        self.realm = try! Realm()
        if let location = self.realm!.objects(Location.self).filter("id = '\(id)'").first {
            return location
        }
        return Location()
    }
    
    public func getBy(name: String) -> Location{
        self.realm = try! Realm()
        if let location = self.realm!.objects(Location.self).filter("name = '\(name)'").first {
            return location
        }
        return Location()
    }
    
    public func getAll() -> Results<Location> {
        self.realm = try! Realm()
        return self.realm!.objects(Location.self)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(Location.self))!)
        }
    }
    
    
}
