//
//  City.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/26/17.
//
//

import Foundation
import RealmSwift

public class City: Object {
    dynamic public var id = ""
    dynamic public var stateId = ""
    dynamic public var name = ""
    override static public func primaryKey() -> String? {
        return "id"
    }
}

public class CityController {
    
    public static let shared = CityController()
    private var realm: Realm?
    
    func put(id: String, stateId:String, name: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(City.self, value: ["id":id, "stateId": stateId, "name":name], update: true)
        }
    }
    
    public func getBy(id: String) -> City{
        self.realm = try! Realm()
        if let city = self.realm!.objects(City.self).filter("id = '\(id)'").first {
            return city
        }
        return City()
    }
    
    public func getBy(name: String) -> City{
        self.realm = try! Realm()
        if let city = self.realm!.objects(City.self).filter("name = '\(name)'").first {
            return city
        }
        return City()
    }
    
    public func getAll() -> Results<City> {
        self.realm = try! Realm()
        return self.realm!.objects(City.self)
    }
    
    public func getAllBy(stateId: String) -> Results<City> {
        self.realm = try! Realm()
        return self.realm!.objects(City.self).filter("stateId = '\(stateId)'")
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(City.self))!)
        }
    }
    
    
}
