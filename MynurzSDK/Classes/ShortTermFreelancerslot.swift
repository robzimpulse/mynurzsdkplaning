//
//  ShortTermFreelancerslot.swift
//  Pods
//
//  Created by Robyarta on 4/27/17.
//
//

import Foundation
import RealmSwift

public class ShortTermFreelancerSlot: Object {
    dynamic public var date = ""
}

public class ShortTermFreelancerSlotController {
    
    public static let shared = ShortTermFreelancerSlotController()
    private var realm: Realm?
    
    func put(date: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            let currentShortTermFreelancerSlot = ShortTermFreelancerSlot()
            currentShortTermFreelancerSlot.date = date
            self.realm!.add(currentShortTermFreelancerSlot)
        }
    }
    
    public func getAll() -> Results<ShortTermFreelancerSlot> {
        self.realm = try! Realm()
        return self.realm!.objects(ShortTermFreelancerSlot.self)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(ShortTermFreelancerSlot.self))!)
        }
    }
    
}
