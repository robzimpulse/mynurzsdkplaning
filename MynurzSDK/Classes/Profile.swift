//
//  Profile.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation
import RealmSwift

public class Profile: Object{
    dynamic public var firstName = ""
    dynamic public var lastName = ""
    dynamic public var email = ""
    dynamic public var country_phone_code = ""
    dynamic public var phone = ""
    dynamic public var imageUrl = ""
    dynamic public var nationality = ""
}

public class ProfileController {
    
    public static let shared = ProfileController()
    private var realm: Realm?
    
    public func get() -> Profile? {
        self.realm = try! Realm()
        return self.realm!.objects(Profile.self).first
    }
    
    func put(firstName: String, lastName: String, email: String,country_phone_code: String, phone: String, imageUrl: String, nationality: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            let currentProfile = Profile()
            currentProfile.firstName = firstName
            currentProfile.lastName = lastName
            currentProfile.email = email
            currentProfile.phone = phone
            currentProfile.country_phone_code = country_phone_code
            currentProfile.imageUrl = imageUrl
            currentProfile.nationality = nationality
            if let oldProfile = self.realm!.objects(Profile.self).first {
                self.realm?.delete(oldProfile)
            }
            self.realm!.add(currentProfile)
        }
    }
    
}
