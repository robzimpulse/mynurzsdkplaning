//
//  Country.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation
import RealmSwift

public class Country: Object{
    dynamic public var id = ""
    dynamic public var name = ""
    dynamic public var code = ""
    dynamic public var iso3 = ""
    dynamic public var numCode = ""
    dynamic public var phoneCode = ""
    dynamic public var isEnable = ""
    
    override static public func primaryKey() -> String? {
        return "id"
    }
    
}

public class CountryController {
    
    public static let shared = CountryController()
    private var realm: Realm?
    
    func put(id: String, name: String, code: String, iso3: String, numCode: String, phoneCode: String, isEnable: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(Country.self, value: [
                "id":id, "name":name, "code":code, "iso3":iso3, "numCode":numCode, "phoneCode":phoneCode, "isEnable":isEnable
                ], update: true)
        }
    }
    
    public func getBy(id: String) -> Country{
        self.realm = try! Realm()
        if let country = self.realm!.objects(Country.self).filter("id = '\(id)'").first {
            return country
        }
        return Country()
    }
    
    public func getBy(iso3: String) -> Country{
        self.realm = try! Realm()
        if let country = self.realm!.objects(Country.self).filter("iso3 = '\(iso3)'").first {
            return country
        }
        return Country()
    }
    
    public func getBy(name: String) -> Country{
        self.realm = try! Realm()
        if let country = self.realm!.objects(Country.self).filter("name = '\(name)'").first {
            return country
        }
        return Country()
    }
    
    public func getAllEnabled() -> Results<Country> {
        self.realm = try! Realm()
        return self.realm!.objects(Country.self).filter("isEnable = '1'")
    }
    
    public func getAll() -> Results<Country> {
        self.realm = try! Realm()
        return self.realm!.objects(Country.self)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(Country.self))!)
        }
    }
    
}
