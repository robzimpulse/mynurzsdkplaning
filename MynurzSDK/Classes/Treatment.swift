//
//  Treatment.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation
import RealmSwift

public class Treatment: Object {
    dynamic public var id = ""
    dynamic public var name = ""
    override static public func primaryKey() -> String? {
        return "id"
    }
}

public class TreatmentController {
    
    public static let shared = TreatmentController()
    private var realm: Realm?
    
    func put(id: String, name: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(Treatment.self, value: ["id":id, "name":name], update: true)
        }
    }
    
    public func getBy(id: String) -> Treatment{
        self.realm = try! Realm()
        if let location = self.realm!.objects(Treatment.self).filter("id = '\(id)'").first {
            return location
        }
        return Treatment()
    }
    
    public func getBy(name: String) -> Treatment{
        self.realm = try! Realm()
        if let location = self.realm!.objects(Treatment.self).filter("name = '\(name)'").first {
            return location
        }
        return Treatment()
    }
    
    public func getAll() -> Results<Treatment> {
        self.realm = try! Realm()
        return self.realm!.objects(Treatment.self)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(Treatment.self))!)
        }
    }
    
    
}
