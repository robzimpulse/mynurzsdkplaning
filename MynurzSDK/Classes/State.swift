//
//  State.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/26/17.
//
//

import Foundation
import RealmSwift

public class State: Object{
    dynamic public var id = ""
    dynamic public var name = ""
    
    override static public func primaryKey() -> String? {
        return "id"
    }
    
}

public class StateController {
    
    public static let shared = StateController()
    private var realm: Realm?
    
    func put(id: String, name: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(State.self, value: ["id":id, "name":name], update: true)
        }
    }
    
    public func getBy(id: String) -> State{
        self.realm = try! Realm()
        if let state = self.realm!.objects(State.self).filter("id = '\(id)'").first {
            return state
        }
        return State()
    }
    
    public func getBy(name: String) -> State{
        self.realm = try! Realm()
        if let state = self.realm!.objects(State.self).filter("name = '\(name)'").first {
            return state
        }
        return State()
    }
    
    public func getAll() -> Results<State> {
        self.realm = try! Realm()
        return self.realm!.objects(State.self)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(State.self))!)
        }
    }
    
}
