//
//  ShortTermFreelancer.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation
import RealmSwift

public class ShortTermFreelancer: Object {
    dynamic public var id = ""
    dynamic public var age = ""
    dynamic public var firstname = ""
    dynamic public var gender = ""
    dynamic public var lastname = ""
    dynamic public var lowestPackageRate = ""
    dynamic public var lowestShiftRate = ""
    dynamic public var photoUrl = ""
    dynamic public var yearExperience = ""
    
    override static public func primaryKey() -> String? {
        return "id"
    }
}

public class ShortTermFreelancerController {
    
    public static let shared = ShortTermFreelancerController()
    private var realm: Realm?
    
    func put(id: String, firstname: String, gender: String, age: String, lastname: String, lowestPackagePrice: String, lowestShiftRate: String, photoUrl: String, yearExperience: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            let currentShortTermFreelancer = ShortTermFreelancer()
            currentShortTermFreelancer.id = id
            currentShortTermFreelancer.age = age
            currentShortTermFreelancer.firstname = firstname
            currentShortTermFreelancer.gender = gender
            currentShortTermFreelancer.lastname = lastname
            currentShortTermFreelancer.lowestShiftRate = lowestShiftRate
            currentShortTermFreelancer.lowestPackageRate = lowestPackagePrice
            currentShortTermFreelancer.photoUrl = photoUrl
            currentShortTermFreelancer.yearExperience = yearExperience
            self.realm!.add(currentShortTermFreelancer)
        }
    }
    
    public func getBy(id: String) -> ShortTermFreelancer{
        self.realm = try! Realm()
        if let location = self.realm!.objects(ShortTermFreelancer.self).filter("id = '\(id)'").first {
            return location
        }
        return ShortTermFreelancer()
    }
    
    public func getAll() -> Results<ShortTermFreelancer> {
        self.realm = try! Realm()
        return self.realm!.objects(ShortTermFreelancer.self)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(ShortTermFreelancer.self))!)
        }
    }
    
}
