//
//  Skill.swift
//  Pods
//
//  Created by Robyarta on 4/28/17.
//
//

import Foundation
import RealmSwift

public class DashboardFreelancer: Object{
    dynamic public var availableToTravel = ""
    dynamic public var revenue = ""
    dynamic public var packagePrice = ""
    dynamic public var minRate = ""
    dynamic public var language = ""
    dynamic public var workingArea = ""
}

public class SkillFreelancer: Object {
    dynamic public var name = ""
    dynamic public var experience = ""
    
    override public static func primaryKey() -> String? {
        return "name"
    }
    
}

public class BiddingJobsFreelancer: Object {
    dynamic public var id = ""
    dynamic public var bidPrice = ""
    dynamic public var debug = ""
    dynamic public var myDescription = ""
    dynamic public var createdAt = ""
    
    override public static func primaryKey() -> String? {
        return "id"
    }
    
}

public class DashboardFreelancerController {
    
    public static let shared = DashboardFreelancerController()
    private var realm: Realm?
    
    func put(availableToTravel: String, revenue: String, packagePrice: String, minRate: String, language: String, workingArea: String){
        try! self.realm!.write {
            let currentData = DashboardFreelancer()
            currentData.availableToTravel = availableToTravel
            currentData.revenue = revenue
            currentData.packagePrice = packagePrice
            currentData.minRate = minRate
            currentData.language = language
            currentData.workingArea = workingArea
            if let oldData = self.realm!.objects(DashboardFreelancer.self).first {
                self.realm?.delete(oldData)
            }
            self.realm!.add(currentData)
        }
    }
    
    func putSkill(name: String, experience: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(SkillFreelancer.self, value: [
                "name":name,"experience":experience
            ], update:true)
        }
    }
    
    func putBiddingJob(id: String, bidPrice: String, debug: String, myDescription: String, createdAt: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(BiddingJobsFreelancer.self, value: [
                "id":id,"bidPrice":bidPrice,"debug":debug,"myDescription":myDescription,"createdAt":createdAt
            ], update:true)
        }
    }
    
    public func get() -> DashboardFreelancer {
        self.realm = try! Realm()
        if let dashboard = self.realm!.objects(DashboardFreelancer.self).first {
            return dashboard
        }
        return DashboardFreelancer()
    }
    
    public func getSkillFrelancer() -> SkillFreelancer {
        self.realm = try! Realm()
        if let dashboard = self.realm!.objects(SkillFreelancer.self).first {
            return dashboard
        }
        return SkillFreelancer()
    }
    
    public func getBiddingJob() -> BiddingJobsFreelancer {
        self.realm = try! Realm()
        if let dashboard = self.realm!.objects(BiddingJobsFreelancer.self).first {
            return dashboard
        }
        return BiddingJobsFreelancer()
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(DashboardFreelancer.self))!)
            self.realm?.delete((self.realm?.objects(SkillFreelancer.self))!)
            self.realm?.delete((self.realm?.objects(BiddingJobsFreelancer.self))!)
        }
    }
    
}
