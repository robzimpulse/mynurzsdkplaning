//
//  MynurzFreelancer.swift
//  Pods
//
//  Created by Robyarta on 4/27/17.
//
//

import Alamofire
import RealmSwift
import SwiftyJSON
import EZSwiftExtensions

public class MynurzFreelancer {

    public var delegate: MynurzFreelancerDelegate?
    private var realm: Realm?
    
    var API_URL_HOST = "http://mynurzcom.app"
    
    private let API_URL_BASE = "/api"
    private let API_URL_VERSION = "/v1"
    private let API_URL_LOGIN = "/login"
    private let API_URL_LOGOUT = "/logout"
    private let API_URL_FREELANCER = "/freelancer"
    private let API_URL_REGISTER = "/register"
    private let API_URL_REFRESH_TOKEN = "/refresh-token"
    private let API_URL_RESET = "/reset"
    private let API_URL_PAYLOAD = "/payload"
    private let API_URL_CHECK = "/status"
    private let API_URL_SETTING = "/setting"
    private let API_URL_PROFILE = "/profile"
    private let API_URL_PROFILE_PICTURE = "/profile-picture"
    private let API_URL_PASSWORD = "/password"
    private let API_URL_DASHBOARD = "/dashboard"
    
    var sessionManager = SessionManager()
    
    public static let local = MynurzFreelancer(state: .Homestead)
    public static let staging = MynurzFreelancer(state: .Staging)
    public static let live = MynurzFreelancer(state: .Live)
    
    init(state: MynurzStateDevelopment) {
        
        Realm.Configuration.defaultConfiguration = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
        
        switch state {
        case .Live:
            self.API_URL_HOST = "https://mynurz.com"
            break
        case .Staging:
            self.API_URL_HOST = "https://staging.mynurz.com"
            break
        default:break
        }
        
        self.sessionManager.adapter = AccessTokenAdapter(accessToken: "")
        if let token = TokenController.shared.get() {
            self.sessionManager.adapter = AccessTokenAdapter(accessToken: token.token)
        }
        
        guard let networkManager = NetworkReachabilityManager(host: self.API_URL_HOST) else {
            print("Network manager not listening")
            return
        }
        networkManager.listener = { status in
            print("Connection status : \(status as Any)")
            if let delegate = self.delegate {
                
                if status == NetworkReachabilityManager.NetworkReachabilityStatus.notReachable {
                    delegate.responseError(message: "network-unreachable".localized(), code: .None, errorCode: .NoNetwork, data: nil)
                    return
                }
                
                if status == NetworkReachabilityManager.NetworkReachabilityStatus.unknown {
                    delegate.responseError(message: "network-unreachable".localized(), code: .None, errorCode: .NoNetwork, data: nil)
                    return
                }
                
            }
        }
        networkManager.startListening()
    }

    private func updateRealm(code: MynurzFreelancerRequestCode, data: JSON){
        switch code {
        case .Login:
            let token = data["token"].string ?? ""
            TokenController.shared.put(token: token)
            self.sessionManager.adapter = AccessTokenAdapter(accessToken: token)
            break
        case .Dashboard:
            DashboardFreelancerController.shared.drop()
            let availableToTravel = data["available_to_travel"].string ?? ""
            let revenue = data["revenue"].string ?? ""
            let packagePrice = data["package_price"].string ?? ""
            let minRate = data["min_rate"].string ?? ""
            let language = data["language"].string ?? ""
            let workingArea = data["working_area"].string ?? ""
            DashboardFreelancerController.shared.put(availableToTravel: availableToTravel, revenue: revenue, packagePrice: packagePrice, minRate: minRate, language: language, workingArea: workingArea)
            if let skills = data["skills"].array {
                for skill in skills {
                    let name = skill["skill_name"].string ?? ""
                    let experience = skill["skill_experience"].string ?? ""
                    DashboardFreelancerController.shared.putSkill(name: name, experience: experience)
                }
            }
            if let biddingJobs = data["bidding_jobs"].array {
                for job in biddingJobs {
                    let id = job["id"].string ?? ""
                    let bidPrice = job["bid_price"].string ?? ""
                    let debug = job["debug"].string ?? ""
                    let myDescription = job["description"].string ?? ""
                    let createdAt = job["created_at"].string ?? ""
                    DashboardFreelancerController.shared.putBiddingJob(id: id, bidPrice: bidPrice, debug: debug, myDescription: myDescription, createdAt: createdAt)
                }
            }
            // MARK : - TODO
            print(data["today_jobs"] as Any)
            print(data["ongoing_jobs"] as Any)
            break
        case .GetProfile:
            let aboutMe = data["about_me"].string ?? ""
            let email = data["email"].string ?? ""
            let fullName = data["full_name"].string ?? ""
            let nameId = data["nameid"].string ?? ""
            let address = data["address"].string ?? ""
            let addressDistrict = data["address_distric"].string ?? ""
            let addressCountry = data["address_country"].string ?? ""
            let gender = data["gender"].string ?? ""
            let addressState = data["address_state"].string ?? ""
            let religion = data["religion"].string ?? ""
            let addressCity = data["address_city"].string ?? ""
            let mobilePhone = data["mobile_phone"].string ?? ""
            let phone = data["phone"].string ?? ""
            let idCardUrl = data["id_card_url"].string ?? ""
            let addressZip = data["address_zip"].string ?? ""
            let dob = data["dob"].string ?? ""
            ProfileFreelancerController.shared.put(nameId: nameId, fullName: fullName, aboutMe: aboutMe, gender: gender, dob: dob, religion: religion, address: address, addressDistrict: addressDistrict, addressCity: addressCity, addressZip: addressZip, addressState: addressState, addressCountry: addressCountry, email: email, mobilePhone: mobilePhone, phone: phone, idCardUrl: idCardUrl)
            break
        case .GetSetting:
            if let countries = data["countries"].array {
                CountryController.shared.drop()
                for country in countries {
                    let id = country["id"].string ?? ""
                    let name = country["country_name"].string ?? ""
                    let code = country["country_code"].string ?? ""
                    let iso3 = country["country_code_iso3"].string ?? ""
                    let numCode = country["country_numcode"].string ?? ""
                    let phoneCode = country["country_phonecode"].string ?? ""
                    let isEnable = country["is_enable"].string ?? ""
                    CountryController.shared.put(id: id, name: name, code: code, iso3: iso3, numCode: numCode, phoneCode: phoneCode, isEnable: isEnable)
                }
            }
            if let languages = data["languages"].array {
                LanguageController.shared.drop()
                for language in languages {
                    let id = language["id"].string ?? ""
                    let name = language["id"].string ?? ""
                    LanguageController.shared.put(id: id, name: name)
                }
            }
            
            if let translations = data["translations"].array {
                TranslationController.shared.drop()
                for translation in translations {
                    let id = translation["id"].string ?? ""
                    let name = translation["name"].string ?? ""
                    TranslationController.shared.put(id: id, name: name)
                }
            }
            if let professions = data["professions"].array {
                ProfessionController.shared.drop()
                for profession in professions {
                    let id = profession["id"].string ?? ""
                    let name = profession["name"].string ?? ""
                    ProfessionController.shared.put(id: id, name: name)
                }
            }
            if let locations = data["locations"].array {
                LocationController.shared.drop()
                for location in locations {
                    let id = location["id"].string ?? ""
                    let name = location["name"].string ?? ""
                    LocationController.shared.put(id: id, name: name)
                }
            }
            if let services = data["services"].array {
                ServiceController.shared.drop()
                for service in services {
                    let id = service["id"].string ?? ""
                    let name = service["name"].string ?? ""
                    ServiceController.shared.put(id: id, name: name)
                }
            }
            if let treatments = data["treatments"].array {
                TreatmentController.shared.drop()
                for treatment in treatments {
                    let id = treatment["id"].string ?? ""
                    let name = treatment["name"].string ?? ""
                    TreatmentController.shared.put(id: id, name: name)
                }
            }
            if let states = data["states"].array {
                StateController.shared.drop()
                for state in states {
                    let id = state["id"].string ?? ""
                    let name = state["name"].string ?? ""
                    StateController.shared.put(id: id, name: name)
                }
            }
            if let cities = data["cities"].array {
                CityController.shared.drop()
                for city in cities {
                    let id = city["id"].string ?? ""
                    let name = city["name"].string ?? ""
                    let stateId = city["state_id"].string ?? ""
                    CityController.shared.put(id: id, stateId: stateId, name: name)
                }
            }
            if let cities = data["districts"].array {
                DistrictController.shared.drop()
                for city in cities {
                    let id = city["id"].string ?? ""
                    let name = city["name"].string ?? ""
                    let cityId = city["city_id"].string ?? ""
                    DistrictController.shared.put(id: id, cityId: cityId, name: name)
                }
            }
            break
        case .Logout:
            self.wipeData()
            break
        default:
            print("Unhandled realm operation for code: \(code)")
            break
        }
    }
    
    public func wipeData(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.deleteAll()
        }
    }
    
    private func request(method: HTTPMethod, url: String, parameters: [String:String]?, code: MynurzFreelancerRequestCode){
        let start = DispatchTime.now()
        self.sessionManager
            .request(url, method: method, parameters: parameters, encoding: URLEncoding.default)
            .responseJSON{ response in
                let stop = DispatchTime.now()
                let timeInterval = Double(stop.uptimeNanoseconds - start.uptimeNanoseconds) / 1_000_000_000
                
                guard let validDelegate = self.delegate else {
                    print("No delegate attached")
                    return
                }
                
                guard let validResponse = response.response else {
                    validDelegate.responseError(message: "Empty response", code: code, errorCode: .InvalidResponseData, data: nil)
                    return
                }
                
                guard let validData = response.data else {
                    validDelegate.responseError(message: "Empty response body", code: code, errorCode: .InvalidResponseData, data: nil)
                    return
                }
                
                let json = JSON(data: validData)
                
                guard let status = json["status"].bool else {
                    validDelegate.responseError(message: "Invalid response body for status", code: code, errorCode: .InvalidResponseData, data: nil)
                    return
                }
                
                guard let message = json["message"].string else {
                    validDelegate.responseError(message: "Invalid response body for message", code: code, errorCode: .InvalidResponseData, data: nil)
                    return
                }
                
                print("\(method.self) \(url.self) \(validResponse.statusCode) \(code) \(timeInterval)")
                if validResponse.statusCode >= 200 && validResponse.statusCode < 500 {
                    if(status){
                        self.updateRealm(code: code, data: json["data"])
                        validDelegate.responseSuccess(message: message, code: code, data: json["data"])
                        return
                    }
                    validDelegate.responseError(message: message, code: code, errorCode: .RejectedByServer, data: json["data"])
                    return
                }
                validDelegate.responseError(message: message, code: code, errorCode: .RequestError, data: json["data"])
                return
        }
    }

    
    // MARK : -
    
    public func login(email: String, password: String) {
        let urlString = self.API_URL_HOST + self.API_URL_BASE + self.API_URL_VERSION + self.API_URL_LOGIN
        let params = ["email" : email, "password" : password]
        self.request(method: .post, url: urlString, parameters: params, code: .Login)
    }
    
    public func getDashboard(){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_FREELANCER + API_URL_DASHBOARD
        self.request(method: .get, url: urlString, parameters: nil, code: .Dashboard)
    }
    
    public func getProfile(){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_FREELANCER + API_URL_PROFILE
        self.request(method: .get, url: urlString, parameters: nil, code: .GetProfile)
    }
    
    public func getSetting(){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_SETTING
        self.request(method: .get, url: urlString, parameters: nil, code: .GetSetting)
    }
    
    public func updateProfile(firstName: String, lastName: String, phone: String, dob: String, gender: String, religion: String, address: String, addressCity: String, addressState: String, addressDistrict: String, addressZip: String, countryId: String){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_FREELANCER + API_URL_PROFILE
        let params = ["first_name":firstName,"last_name":lastName,"mobile_phone":phone,"dob": dob,"gender": gender,"religion":religion,"address":address,"address_city":addressCity,"address_state":addressState,"address_district":addressDistrict,"address_zip":addressZip,"country_id":countryId]
        self.request(method: .post, url: urlString, parameters: params, code: .UpdateProfile)
    }
    
    public func logout(){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_LOGOUT
        self.request(method: .get, url: urlString, parameters: nil, code: .Logout)
    }
}
