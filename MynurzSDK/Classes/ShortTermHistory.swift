//
//  ShortTermHistory.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation
import RealmSwift

public class ShortTermHistory: Object {
    dynamic public var id = ""
    dynamic public var amount = ""
    dynamic public var created_at = ""
    dynamic public var myDescription = ""
    dynamic public var nameid = ""
    dynamic public var provider = ""
    dynamic public var status = ""
    dynamic public var timestamp = ""
    
    override static public func primaryKey() -> String? {
        return "id"
    }
}

public class ShortTermHistoryController {
    
    public static let shared = ShortTermHistoryController()
    private var realm: Realm?
    
    func put(id: String, amount: String, created_at: String, myDescription: String, nameid: String, provider: String, status: String, timestamp: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(ShortTermHistory.self, value: ["id":id, "amount":amount, "created_at":created_at, "myDescription":myDescription, "nameid": nameid, "provider": provider, "status": status, "timestamp": timestamp
                ], update: true)
        }
    }
    
    public func getBy(id: String) -> ShortTermHistory?{
        self.realm = try! Realm()
        return self.realm!.objects(ShortTermHistory.self).filter("id = '\(id)'").first
    }
    
    public func getAll() -> Results<ShortTermHistory> {
        self.realm = try! Realm()
        return self.realm!.objects(ShortTermHistory.self).sorted(byKeyPath: "timestamp", ascending: false)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(ShortTermHistory.self))!)
        }
    }
    
}
