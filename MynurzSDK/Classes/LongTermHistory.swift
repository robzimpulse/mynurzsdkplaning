//
//  LongTermHistory.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/26/17.
//
//

import Foundation
import RealmSwift

public class LongTermHistory: Object {
    dynamic public var id = ""
    dynamic public var customerId = ""
    dynamic public var agree = ""
    dynamic public var status = ""
    dynamic public var type = ""
    dynamic public var created_at = ""
    dynamic public var patientId = ""
    dynamic public var patientName = ""
    dynamic public var patientCondition = ""
    dynamic public var patientConditionDetail = ""
    dynamic public var profession = ""
    dynamic public var serviceType = ""
    dynamic public var gender = ""
    dynamic public var totalSession = ""
    dynamic public var startDate = ""
    dynamic public var endDate = ""
    dynamic public var dateSchedule = ""
    dynamic public var myDescription = ""
    dynamic public var totalProposal = ""
    
    override static public func primaryKey() -> String? {
        return "id"
    }
}

public class LongTermHistoryController {
    
    public static let shared = LongTermHistoryController()
    private var realm: Realm?
    
    func put(id: String, customerId: String, agree: String, status: String, type: String, createdAt: String, patientId: String, patientName: String, patientCondition: String, patientConditionDetail: String, profession: String, serviceType: String, gender: String,totalSession: String,startDate: String,endDate: String,dateSchedule: String,myDescription: String,totalProposal: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(LongTermHistory.self, value: [
                "id": id,
                "customerId": customerId,
                "agree": agree,
                "status": status,
                "type": type,
                "createdAt": createdAt,
                "patientId": patientId,
                "patientName": patientName,
                "patientCondition": patientCondition,
                "patientConditionDetail": patientConditionDetail,
                "profession": profession,
                "serviceType": serviceType,
                "gender": gender,
                "totalSession": totalSession,
                "startDate": startDate,
                "endDate": endDate,
                "dateSchedule": dateSchedule,
                "myDescription": myDescription,
                "totalProposal": totalProposal
            ], update: true)
        }
    }
    
    public func getBy(id: String) -> LongTermHistory?{
        self.realm = try! Realm()
        return self.realm!.objects(LongTermHistory.self).filter("id = '\(id)'").first
    }
    
    public func getAll() -> Results<LongTermHistory> {
        self.realm = try! Realm()
        return self.realm!.objects(LongTermHistory.self)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(LongTermHistory.self))!)
        }
    }
    
}
