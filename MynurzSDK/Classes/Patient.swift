//
//  Patient.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation
import RealmSwift

public class Patient: Object{
    dynamic public var id = ""
    dynamic public var name = ""
    dynamic public var relationship = ""
    dynamic public var gender = ""
    dynamic public var age = ""
    dynamic public var height = ""
    dynamic public var weight = ""
    dynamic public var nationality = ""
    
    override public static func primaryKey() -> String? {
        return "id"
    }
}

public class PatientController {
    
    public static let shared = PatientController()
    private var realm: Realm?
    
    func put(id: String, name: String, relationship: String, gender: String, age: String, weight: String, height: String, nationality: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(Patient.self, value: [
                "id":id,"name":name,"relationship":relationship,"gender":gender,"age":age,"weight":weight,"height":height,"nationality":nationality
                ], update: true)
        }
    }
    
    public func getBy(id: String) -> Patient{
        self.realm = try! Realm()
        if let patient = self.realm!.objects(Patient.self).filter("id = '\(id)'").first {
            return patient
        }
        return Patient()
    }
    
    public func getAll() -> Results<Patient> {
        self.realm = try! Realm()
        return self.realm!.objects(Patient.self)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(Patient.self))!)
        }
    }
    
}
