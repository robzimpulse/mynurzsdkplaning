//
//  MynurzCustomer.swift
//  Pods
//
//  Created by Robyarta on 4/27/17.
//
//

import Alamofire
import RealmSwift
import SwiftyJSON
import EZSwiftExtensions

public class MynurzCustomer {
    
    public var delegate: MynurzCustomerDelegate?
    private var realm: Realm?
    
    var API_URL_HOST = "http://mynurzcom.app"
    
    private let API_URL_BASE = "/api"
    private let API_URL_VERSION = "/v1"
    private let API_URL_LOGIN = "/login"
    private let API_URL_CUSTOMER = "/customer"
    private let API_URL_REGISTER = "/register"
    private let API_URL_REFRESH_TOKEN = "/refresh-token"
    private let API_URL_RESET = "/reset"
    private let API_URL_PAYLOAD = "/payload"
    private let API_URL_CHECK = "/status"
    private let API_URL_SETTING = "/setting"
    private let API_URL_LOGOUT = "/logout"
    private let API_URL_PROFILE = "/profile"
    private let API_URL_PROFILE_PICTURE = "/profile-picture"
    private let API_URL_PASSWORD = "/password"
    private let API_URL_TERMS = "/terms"
    private let API_URL_FAQ = "/faq"
    private let API_URL_NEEDS = "/needs"
    private let API_URL_SHORT_TERM = "/short-term"
    private let API_URL_LONG_TERM = "/long-term"
    private let API_URL_HISTORY_SHORT_TERM = "/history-short-term"
    private let API_URL_HISTORY_LONG_TERM = "/history-long-term"
    private let API_URL_PATIENT = "/patient"
    
    var sessionManager = SessionManager()
    
    public static let local = MynurzCustomer(state: .Homestead)
    public static let staging = MynurzCustomer(state: .Staging)
    public static let live = MynurzCustomer(state: .Live)
    
    init(state: MynurzStateDevelopment) {
        
        Realm.Configuration.defaultConfiguration = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
        
        switch state {
        case .Live:
            self.API_URL_HOST = "https://mynurz.com"
            break
        case .Staging:
            self.API_URL_HOST = "https://staging.mynurz.com"
            break
        default:break
        }
        
        self.sessionManager.adapter = AccessTokenAdapter(accessToken: "")
        if let token = TokenController.shared.get() {
            self.sessionManager.adapter = AccessTokenAdapter(accessToken: token.token)
        }
        
        guard let networkManager = NetworkReachabilityManager(host: self.API_URL_HOST) else {
            print("Network manager not listening")
            return
        }
        networkManager.listener = { status in
            print("Connection status : \(status as Any)")
            if let delegate = self.delegate {
                
                if status == NetworkReachabilityManager.NetworkReachabilityStatus.notReachable {
                    delegate.responseError(message: "network-unreachable".localized(), code: .None, errorCode: .NoNetwork, data: nil)
                    return
                }
                
                if status == NetworkReachabilityManager.NetworkReachabilityStatus.unknown {
                    delegate.responseError(message: "network-unreachable".localized(), code: .None, errorCode: .NoNetwork, data: nil)
                    return
                }
                
            }
        }
        networkManager.startListening()
    }
    
    private func updateRealm(code: MynurzCustomerRequestCode, data: JSON){
        switch code {
        case .Login, .UpdateLocale:
            let token = data["token"].string ?? ""
            TokenController.shared.put(token: token)
            self.sessionManager.adapter = AccessTokenAdapter(accessToken: token)
            break
        case .GetLocale:
            let region = data["region"].string ?? ""
            let language = data["language"].string ?? ""
            LocalizationController.shared.put(region: region, language: language)
            break
        case .GetProfile:
            let firstName = data["first_name"].string ?? ""
            let lastName = data["last_name"].string ?? ""
            let email = data["email"].string ?? ""
            let phone = data["phone"].string ?? ""
            let imageUrl = data["image_url"].string ?? ""
            let nationality = data["nationality"].string ?? ""
            let phone_code = data["phone_code"].string ?? ""
            ProfileController.shared.put(firstName: firstName, lastName: lastName, email: email, country_phone_code: phone_code, phone: phone, imageUrl: imageUrl, nationality: nationality)
            break
        case .GetSetting:
            if let countries = data["countries"].array {
                CountryController.shared.drop()
                for country in countries {
                    let id = country["id"].string ?? ""
                    let name = country["country_name"].string ?? ""
                    let code = country["country_code"].string ?? ""
                    let iso3 = country["country_code_iso3"].string ?? ""
                    let numCode = country["country_numcode"].string ?? ""
                    let phoneCode = country["country_phonecode"].string ?? ""
                    let isEnable = country["is_enable"].string ?? ""
                    CountryController.shared.put(id: id, name: name, code: code, iso3: iso3, numCode: numCode, phoneCode: phoneCode, isEnable: isEnable)
                }
            }
            if let languages = data["languages"].array {
                LanguageController.shared.drop()
                for language in languages {
                    let id = language["id"].string ?? ""
                    let name = language["id"].string ?? ""
                    LanguageController.shared.put(id: id, name: name)
                }
            }
            
            if let translations = data["translations"].array {
                TranslationController.shared.drop()
                for translation in translations {
                    let id = translation["id"].string ?? ""
                    let name = translation["name"].string ?? ""
                    TranslationController.shared.put(id: id, name: name)
                }
            }
            if let professions = data["professions"].array {
                ProfessionController.shared.drop()
                for profession in professions {
                    let id = profession["id"].string ?? ""
                    let name = profession["name"].string ?? ""
                    ProfessionController.shared.put(id: id, name: name)
                }
            }
            if let locations = data["locations"].array {
                LocationController.shared.drop()
                for location in locations {
                    let id = location["id"].string ?? ""
                    let name = location["name"].string ?? ""
                    LocationController.shared.put(id: id, name: name)
                }
            }
            if let services = data["services"].array {
                ServiceController.shared.drop()
                for service in services {
                    let id = service["id"].string ?? ""
                    let name = service["name"].string ?? ""
                    ServiceController.shared.put(id: id, name: name)
                }
            }
            if let treatments = data["treatments"].array {
                TreatmentController.shared.drop()
                for treatment in treatments {
                    let id = treatment["id"].string ?? ""
                    let name = treatment["name"].string ?? ""
                    TreatmentController.shared.put(id: id, name: name)
                }
            }
            if let states = data["states"].array {
                StateController.shared.drop()
                for state in states {
                    let id = state["id"].string ?? ""
                    let name = state["name"].string ?? ""
                    StateController.shared.put(id: id, name: name)
                }
            }
            if let cities = data["cities"].array {
                CityController.shared.drop()
                for city in cities {
                    let id = city["id"].string ?? ""
                    let name = city["name"].string ?? ""
                    let stateId = city["state_id"].string ?? ""
                    CityController.shared.put(id: id, stateId: stateId, name: name)
                }
            }
            if let cities = data["districts"].array {
                DistrictController.shared.drop()
                for city in cities {
                    let id = city["id"].string ?? ""
                    let name = city["name"].string ?? ""
                    let cityId = city["city_id"].string ?? ""
                    DistrictController.shared.put(id: id, cityId: cityId, name: name)
                }
            }
            break
        case .GetHistoryShortTerm:
            if let paginateDatas = data["data"].array {
                for singleData in paginateDatas {
                    let id = singleData["id"].string ?? ""
                    let amount = singleData["amount"].string ?? ""
                    let created_at = singleData["created_at"].string ?? ""
                    let myDescription = singleData["description"].string ?? ""
                    let nameid = singleData["nameid"].string ?? ""
                    let provider = singleData["provider"].string ?? ""
                    let status = singleData["status"].string ?? ""
                    let timestamp = singleData["timestamp"].string ?? ""
                    ShortTermHistoryController.shared.put(id: id, amount: amount, created_at: created_at, myDescription: myDescription, nameid: nameid, provider: provider, status: status, timestamp: timestamp)
                }
            }
            break
        case .GetShortTermFreelancer:
            if let paginateDatas = data["freelancers"].array {
                ShortTermFreelancerController.shared.drop()
                for singleData in paginateDatas {
                    let id = singleData["id"].string ?? ""
                    let firstname = singleData["first_name"].string ?? ""
                    let gender = singleData["gender"].string ?? ""
                    let age = singleData["age"].string ?? ""
                    let lastname = singleData["last_name"].string ?? ""
                    let lowestPackageRate = singleData["lowest_package_rate"].string ?? ""
                    let lowestShiftRate = singleData["lowest_shift_rate"].string ?? ""
                    let photoUrl = singleData["photo_url"].string ?? ""
                    let yearExperience = singleData["year_experience"].string ?? ""
                    ShortTermFreelancerController.shared.put(id: id, firstname: firstname, gender: gender, age: age, lastname: lastname, lowestPackagePrice: lowestPackageRate, lowestShiftRate: lowestShiftRate, photoUrl: photoUrl, yearExperience: yearExperience)
                }
            }
            if let paginateDatas = data["slots"].array {
                ShortTermFreelancerSlotController.shared.drop()
                for singleData in paginateDatas {
                    let date = singleData.string ?? ""
                    ShortTermFreelancerSlotController.shared.put(date: date)
                }
            }
            break
        case .GetPatient:
            if let paginateDatas = data["data"].array {
                for singleData in paginateDatas {
                    let id = singleData["id"].string ?? ""
                    let name = singleData["name"].string ?? ""
                    let relationship = singleData["relationship"].string ?? ""
                    let gender = singleData["gender"].string ?? ""
                    let age = singleData["age"].string ?? ""
                    let height = singleData["height"].string ?? ""
                    let weight = singleData["weight"].string ?? ""
                    let nationality = singleData["nationality"].string ?? ""
                    PatientController.shared.put(id: id, name: name, relationship: relationship, gender: gender, age: age, weight: weight, height: height, nationality: nationality)
                }
            }
            break
        case .GetHistoryLongTerm:
            if let paginateDatas = data["data"].array {
                for singleData in paginateDatas {
                    let id = singleData["id"].string ?? ""
                    let totalProposal = singleData["total_proposal"].string ?? ""
                    let profession = singleData["profession"].string ?? ""
                    let patientCondition = singleData["patient_condition"].string ?? ""
                    let patientConditionDetail = singleData["patient_condition_detail"].string ?? ""
                    let totalSession = singleData["total_session"].string ?? ""
                    let patientId = singleData["patient_id"].string ?? ""
                    let myDescription = singleData["myDescription"].string ?? ""
                    let gender = singleData["gender"].string ?? ""
                    let type = singleData["type"].string ?? ""
                    let dateSchedule = singleData["date_schedule"].string ?? ""
                    let customerId = singleData["customer_id"].string ?? ""
                    let status = singleData["status"].string ?? ""
                    let serviceType = singleData["service_type"].string ?? ""
                    let patientName = singleData["patient_name"].string ?? ""
                    let agree = singleData["agree"].string ?? ""
                    let createdAt = singleData["created_at"].string ?? ""
                    let startDate = singleData["start_date"].string ?? ""
                    let endDate = singleData["end_date"].string ?? ""
                    LongTermHistoryController.shared.put(id: id, customerId: customerId, agree: agree, status: status, type: type, createdAt: createdAt, patientId: patientId, patientName: patientName, patientCondition: patientCondition, patientConditionDetail: patientConditionDetail, profession: profession, serviceType: serviceType, gender: gender, totalSession: totalSession, startDate: startDate, endDate: endDate, dateSchedule: dateSchedule, myDescription: myDescription, totalProposal: totalProposal)
                }
            }
            break
        case .Logout:
            self.wipeData()
            break
        default:
            print("Unhandled realm operation for code: \(code)")
            break
        }
    }
    
    public func wipeData(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.deleteAll()
        }
    }
    
    private func request(method: HTTPMethod, url: String, parameters: [String:String]?, code: MynurzCustomerRequestCode){
        let start = DispatchTime.now()
        self.sessionManager
            .request(url, method: method, parameters: parameters, encoding: URLEncoding.default)
            .responseJSON{ response in
                let stop = DispatchTime.now()
                let timeInterval = Double(stop.uptimeNanoseconds - start.uptimeNanoseconds) / 1_000_000_000
                
                guard let validDelegate = self.delegate else {
                    print("No delegate attached")
                    return
                }
                
                guard let validResponse = response.response else {
                    validDelegate.responseError(message: "Empty response", code: code, errorCode: .InvalidResponseData, data: nil)
                    return
                }
                
                guard let validData = response.data else {
                    validDelegate.responseError(message: "Empty response body", code: code, errorCode: .InvalidResponseData, data: nil)
                    return
                }
                
                let json = JSON(data: validData)
                
                guard let status = json["status"].bool else {
                    validDelegate.responseError(message: "Invalid response body for status", code: code, errorCode: .InvalidResponseData, data: nil)
                    return
                }
                
                guard let message = json["message"].string else {
                    validDelegate.responseError(message: "Invalid response body for message", code: code, errorCode: .InvalidResponseData, data: nil)
                    return
                }
                
                print("\(method.self) \(url.self) \(validResponse.statusCode) \(code) \(timeInterval)")
                if validResponse.statusCode >= 200 && validResponse.statusCode < 500 {
                    if(status){
                        self.updateRealm(code: code, data: json["data"])
                        validDelegate.responseSuccess(message: message, code: code, data: json["data"])
                        return
                    }
                    validDelegate.responseError(message: message, code: code, errorCode: .RejectedByServer, data: json["data"])
                    return
                }
                validDelegate.responseError(message: message, code: code, errorCode: .RequestError, data: json["data"])
                return
        }
    }
    
    // MARK : - FAQ URL
    
    public lazy var faqUrl: String = {
        [unowned self] in
        guard let localization = LocalizationController.shared.get() else {
            print("Localization not defined")
            return self.API_URL_HOST + self.API_URL_FAQ
        }
        return self.API_URL_HOST + "/\(localization.language)" + "/\(localization.region)" + self.API_URL_FAQ
        }()
    
    // MARK : - Term of Service URL
    
    public lazy var termOfServiceUrl: String = {
        [unowned self] in
        guard let localization = LocalizationController.shared.get() else {
            print("Localization not defined")
            return self.API_URL_HOST + self.API_URL_TERMS
        }
        return self.API_URL_HOST + "/\(localization.language)" + "/\(localization.region)" + self.API_URL_TERMS
        }()
    
    // MARK : - Public Api
    
    public func login(email: String, password: String) {
        let urlString = self.API_URL_HOST + self.API_URL_BASE + self.API_URL_VERSION + self.API_URL_LOGIN
        let params = ["email" : email, "password" : password]
        self.request(method: .post, url: urlString, parameters: params, code: .Login)
    }
    
    public func resetPassword(email: String){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_RESET
        let params = ["email" : email]
        self.request(method: .post, url: urlString, parameters: params, code: .ResetPassword)
    }
    
    public func registerCustomer(email: String, password: String, firstName: String, mobilePhone: String){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_REGISTER + API_URL_CUSTOMER
        let params = ["email" : email, "password" : password, "first_name" : firstName, "mobile_phone" : mobilePhone, "country_code" : "IDN"]
        self.request(method: .post, url: urlString, parameters: params, code: .RegisterCustomer)
    }
    
    public func updateLocalization(region: String, language: String){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_PAYLOAD
        let params = ["region" : region, "language" : language]
        self.request(method: .post, url: urlString, parameters: params, code: .UpdateLocale)
    }
    
    public func getLocalization(){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_PAYLOAD
        self.request(method: .get, url: urlString, parameters: nil, code: .GetLocale)
    }
    
    public func checkTokenStatus(){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_CHECK
        self.request(method: .get, url: urlString, parameters: nil, code: .CheckToken)
    }
    
    public func updateProfile(firstName: String, lastName: String, phone: String, nationality: String, phone_code: String){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_CUSTOMER + API_URL_PROFILE
        let params = ["first_name":firstName,"last_name":lastName,"mobile_phone":phone,"phone_code": phone_code,"nationality": nationality]
        self.request(method: .post, url: urlString, parameters: params, code: .UpdateProfile)
    }
    
    public func getProfile(){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_CUSTOMER + API_URL_PROFILE
        self.request(method: .get, url: urlString, parameters: nil, code: .GetProfile)
    }
    
    public func changePassword(password: String, confirmPassword: String){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_CUSTOMER + API_URL_PASSWORD
        let params = ["password": password, "password_confirmation": confirmPassword]
        self.request(method: .post, url: urlString, parameters: params, code: .ChangePassword)
    }
    
    public func getSetting(){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_SETTING
        self.request(method: .get, url: urlString, parameters: nil, code: .GetSetting)
    }
    
    public func logout(){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_LOGOUT
        self.request(method: .get, url: urlString, parameters: nil, code: .Logout)
    }
    
    public func getHistoryShortTerm(){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_CUSTOMER + API_URL_NEEDS + API_URL_HISTORY_SHORT_TERM
        self.request(method: .get, url: urlString, parameters: nil, code: .GetHistoryShortTerm)
    }
    
    public func getHistoryLongTerm(){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_CUSTOMER + API_URL_NEEDS + API_URL_HISTORY_LONG_TERM
        self.request(method: .get, url: urlString, parameters: nil, code: .GetHistoryLongTerm)
    }
    
    public func createLongTerm(patientId: String, stateId: String, cityId: String, professionId: String, freelancerGender: String, serviceId: String, session: String, startDate: String, jobDetails: String?, addressDetail: String?){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_CUSTOMER + API_URL_NEEDS + API_URL_LONG_TERM
        
        var params = ["patient_id": patientId, "address_state_id": stateId, "address_city_id": cityId, "freelancer_spec": professionId, "freelancer_gender": freelancerGender, "service_type": serviceId, "session": session, "start_date": startDate]
        
        if let jobdetail = jobDetails {
            params["job_details"] = jobdetail
        }
        
        if let addressdetail = addressDetail {
            params["address_details"] = addressdetail
        }
        
        self.request(method: .post, url: urlString, parameters: params, code: .CreateLongTerm)
    }
    
    public func getShortTermFreelancer(location: String, profession: String, service: String?, treatment: String?){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_CUSTOMER + API_URL_NEEDS + API_URL_SHORT_TERM
        var params = ["location": location, "profession": profession]
        if let paramService = service {
            params["type_service"] = paramService
        }
        if let paramTreatment = treatment {
            params["treatment"] = paramTreatment
        }
        self.request(method: .post, url: urlString, parameters: params, code: .GetShortTermFreelancer)
    }
    
    public func getPatient(page: String = "1"){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_CUSTOMER + API_URL_PATIENT
        self.request(method: .get, url: urlString, parameters: ["page": page], code: .GetPatient)
    }
    
    public func addPatient(name: String, dob: String, gender: String, relationship: String, condition: String, conditionDetail: String, height: String, weight: String, nationality: String){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_CUSTOMER + API_URL_PATIENT
        let params = [
            "name":name,"dob":dob,"gender":gender,"relationship":relationship,"nationality": nationality,
            "condition":condition,"condition_detail":conditionDetail,"height":height,"weight":weight
        ]
        self.request(method: .post, url: urlString, parameters: params, code: .AddPatient)
    }
    
    public func updatePatient(id: String, name: String, dob: String, gender: String, relationship: String, condition: String, conditionDetail: String, height: String, weight: String, nationality: String){
        let urlString = API_URL_HOST + API_URL_BASE + API_URL_VERSION + API_URL_CUSTOMER + API_URL_PATIENT + "/\(id)"
        let params = [
            "name":name,"dob":dob,"gender":gender,"relationship":relationship,"nationality": nationality,
            "condition":condition,"condition_detail":conditionDetail,"height":height,"weight":weight
        ]
        self.request(method: .post, url: urlString, parameters: params, code: .UpdatePatient)
    }
}
