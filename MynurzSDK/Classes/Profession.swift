//
//  Profession.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation
import RealmSwift

public class Profession: Object {
    dynamic public var id = ""
    dynamic public var name = ""
    override static public func primaryKey() -> String? {
        return "id"
    }
}

public class ProfessionController {
    
    public static let shared = ProfessionController()
    private var realm: Realm?
    
    func put(id: String, name: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm!.create(Profession.self, value: ["id":id, "name":name], update: true)
        }
    }
    
    public func getBy(id: String) -> Profession{
        self.realm = try! Realm()
        if let profession = self.realm!.objects(Profession.self).filter("id = '\(id)'").first {
            return profession
        }
        return Profession()
    }
    
    public func getBy(name: String) -> Profession{
        self.realm = try! Realm()
        if let profession = self.realm!.objects(Profession.self).filter("name = '\(name)'").first {
            return profession
        }
        return Profession()
    }
    
    public func getAll() -> Results<Profession> {
        self.realm = try! Realm()
        return self.realm!.objects(Profession.self)
    }
    
    func drop(){
        self.realm = try! Realm()
        try! self.realm!.write {
            self.realm?.delete((self.realm?.objects(Profession.self))!)
        }
    }
    
    
}
