//
//  Token.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//
import Foundation
import RealmSwift

public class Token: Object{
    dynamic public var token = ""
}

public class TokenController {
    
    public static let shared = TokenController()
    private var realm: Realm?
    
    public func get() -> Token? {
        self.realm = try! Realm()
        return self.realm!.objects(Token.self).first
    }
    
    func put(token: String){
        self.realm = try! Realm()
        try! self.realm!.write {
            let currentToken = Token()
            currentToken.token = token
            if let oldToken = self.realm!.objects(Token.self).first {
                self.realm?.delete(oldToken)
            }
            self.realm!.add(currentToken)
        }
    }
    
}
