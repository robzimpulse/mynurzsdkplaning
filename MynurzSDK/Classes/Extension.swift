//
//  Extension.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/25/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Foundation

extension String {
    
    func localized() -> String{
        if let translation = TranslationController.shared.getBy(id: self) {
            return translation.name
        }
        return self
    }
    
}

extension NSObject {
    
    func getDataAsString(key: String) -> String {
        guard let value = self.value(forKey: key) else {
            return ""
        }
        guard let valueAsString = value as? String else {
            return ""
        }
        return valueAsString
    }
    
}
