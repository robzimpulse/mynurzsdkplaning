//
//  MynurzProtocol.swift
//  Pods
//
//  Created by Robyarta on 4/28/17.
//
//

import Alamofire
import RealmSwift
import SwiftyJSON
import EZSwiftExtensions

public enum MynurzCustomerRequestCode: Int {
    case None = 100000,
    
    CheckToken,
    Login,
    RegisterCustomer,
    GetSetting,
    ChangePassword,
    ResetPassword,
    GetLocale,
    UpdateLocale,
    GetProfile,
    UpdateProfile,
    UpdateProfilePicture,
    GetHistoryShortTerm,
    GetHistoryLongTerm,
    GetShortTermFreelancer,
    Logout,
    AddPatient,
    GetPatient,
    UpdatePatient,
    UploadProgress,
    CreateLongTerm
}

public enum MynurzFreelancerRequestCode: Int {
    case None = 100000,
    
    Login,
    Dashboard,
    GetSetting,
    GetProfile,
    UpdateProfile,
    Logout
}


public enum MynurzErrorCode: Int {
    case None = 100000,
    InvalidResponseData,
    RejectedByServer,
    RequestError,
    NoNetwork
}

public enum MynurzStateDevelopment: Int {
    case Live = 100000,
    Staging,
    Homestead
}

public protocol MynurzCustomerDelegate {
    func responseError(message: String, code: MynurzCustomerRequestCode, errorCode: MynurzErrorCode, data: JSON?)
    func responseSuccess(message: String, code: MynurzCustomerRequestCode, data: JSON)
}

public protocol MynurzFreelancerDelegate {
    func responseError(message: String, code: MynurzFreelancerRequestCode, errorCode: MynurzErrorCode, data: JSON?)
    func responseSuccess(message: String, code: MynurzFreelancerRequestCode, data: JSON)
}
