# MynurzSDK

[![CI Status](http://img.shields.io/travis/kugelfang.killaruna@gmail.com/MynurzSDK.svg?style=flat)](https://travis-ci.org/kugelfang.killaruna@gmail.com/MynurzSDK)
[![Version](https://img.shields.io/cocoapods/v/MynurzSDK.svg?style=flat)](http://cocoapods.org/pods/MynurzSDK)
[![License](https://img.shields.io/cocoapods/l/MynurzSDK.svg?style=flat)](http://cocoapods.org/pods/MynurzSDK)
[![Platform](https://img.shields.io/cocoapods/p/MynurzSDK.svg?style=flat)](http://cocoapods.org/pods/MynurzSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MynurzSDK is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MynurzSDK', :git => 'https://robzimpulse@bitbucket.org/robzimpulse/mynurzsdk.git', :tag => '0.3.2'
```

## Author

kugelfang.killaruna@gmail.com, kugelfang.killaruna@gmail.com

## License

MynurzSDK is available under the MIT license. See the LICENSE file for more info.
