Pod::Spec.new do |s|
  s.name             = 'MynurzSDK'
  s.version          = '0.3.2'
  s.summary          = 'Framework for accessing mynurz endpoint.'

  s.homepage         = 'https://robzimpulse@bitbucket.org/robzimpulse/mynurzsdk.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'kugelfang.killaruna@gmail.com' => 'kugelfang.killaruna@gmail.com' }
  s.source           = { :git => 'https://robzimpulse@bitbucket.org/robzimpulse/mynurzsdk.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3' }
  s.source_files = 'MynurzSDK/Classes/**/*'

  s.dependency "Alamofire"
  s.dependency "AlamofireImage"
  s.dependency "RealmSwift"
  s.dependency "SwiftyJSON"
  s.dependency "AlamofireImage"
  s.dependency "EZSwiftExtensions"

end
