//
//  FreelancerTest.swift
//  MynurzSDK
//
//  Created by Robyarta on 4/27/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import Quick
import Nimble
import RealmSwift
import MynurzSDK
import Foundation

class FreelancerTests: QuickSpec {
    
    override func spec() {
        
        let sdk = MynurzFreelancer.local
        let mock = MockSDK()
        sdk.delegate = mock
        sdk.wipeData()
        
        describe("Freelancer testing feature") {
            
            beforeEach {
                sdk.login(email: "iqbal@kronusasia.com", password: "11111")
                expect(mock.lastCodeB).toEventually(equal(MynurzFreelancerRequestCode.Login),timeout: 30.0)
                expect(TokenController.shared.get()).notTo(beNil())
            }
            
            afterEach {
                sdk.logout()
                expect(mock.lastCodeB).toEventually(equal(MynurzFreelancerRequestCode.Logout),timeout: 30.0)
                expect(TokenController.shared.get()).to(beNil())
            }
            
            it("dashboard") {
                sdk.getDashboard()
                expect(mock.lastCodeB).toEventually(equal(MynurzFreelancerRequestCode.Dashboard),timeout: 30.0)
                expect(DashboardFreelancerController.shared.get()).notTo(beNil())
                expect(DashboardFreelancerController.shared.getSkillFrelancer()).notTo(beNil())
                expect(DashboardFreelancerController.shared.getBiddingJob()).notTo(beNil())
            }
            
            it("profile") {
                sdk.getProfile()
                expect(mock.lastCodeB).toEventually(equal(MynurzFreelancerRequestCode.GetProfile),timeout: 30.0)
                expect(ProfileFreelancerController.shared.get()).notTo(beNil())
            }
            
            it("update profile") {
                sdk.updateProfile(firstName: "Iqbal", lastName: "Bayumurti", phone: "+6281222542154", dob: "1993-12-12", gender: "male", religion: "1", address: "Testing fake address", addressCity: "26065", addressState: "25823", addressDistrict: "26120", addressZip: "14431", countryId: "100")
                expect(mock.lastCodeB).toEventually(equal(MynurzFreelancerRequestCode.UpdateProfile),timeout: 30.0)
            }
            
            it("get application setting") {
                sdk.getSetting()
                expect(mock.lastCodeB).toEventually(equal(MynurzFreelancerRequestCode.GetSetting),timeout: 30.0)
                expect(CountryController.shared.getAll().count).notTo(equal(0))
                expect(LanguageController.shared.getAll().count).notTo(equal(0))
                expect(TranslationController.shared.getAll().count).notTo(equal(0))
                expect(LocationController.shared.getAll().count).notTo(equal(0))
                expect(ProfessionController.shared.getAll().count).notTo(equal(0))
                expect(ServiceController.shared.getAll().count).notTo(equal(0))
                expect(TreatmentController.shared.getAll().count).notTo(equal(0))
                expect(StateController.shared.getAll().count).notTo(equal(0))
                expect(CityController.shared.getAll().count).notTo(equal(0))
                expect(DistrictController.shared.getAll().count).notTo(equal(0))

            }
            
        }
    }
}
