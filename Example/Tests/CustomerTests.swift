//
//  basicTests.swift
//  basicTests
//
//  Created by Robyarta on 4/24/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import Quick
import Nimble
import RealmSwift
import MynurzSDK
import Foundation

class CustomerTests: QuickSpec {
    
    override func spec() {
        
        let sdk = MynurzCustomer.local
        let mock = MockSDK()
        sdk.delegate = mock
        sdk.wipeData()
        
        describe("Customer Authentication") {
            it("register") {
                sdk.registerCustomer(email: "beahan.oran@example.org", password: "kiasu123", firstName: "testing user", mobilePhone: "+6281222542156")
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.RegisterCustomer), timeout: 30.0)
            }
            
            it("request reset password link") {
                sdk.resetPassword(email: "beahan.oran@example.org")
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.ResetPassword), timeout: 30.0)
            }
        }
        
        describe("Customer Main Feature") {
            
            beforeEach {
                sdk.login(email: "beahan.oran@example.org", password: "kiasu123")
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.Login),timeout: 30.0)
                expect(TokenController.shared.get()).notTo(beNil())

            }
            
            afterEach {
                sdk.logout()
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.Logout),timeout: 30.0)
                expect(TokenController.shared.get()).to(beNil())
            }
            
            it("check token") {
                sdk.checkTokenStatus()
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.CheckToken), timeout: 30.0)
                expect(TokenController.shared.get()).notTo(beNil())
            }
            
            it("get setting application") {
                sdk.getSetting()
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.GetSetting), timeout: 30.0)
                expect(CountryController.shared.getAll().count).notTo(equal(0))
                expect(LanguageController.shared.getAll().count).notTo(equal(0))
                expect(TranslationController.shared.getAll().count).notTo(equal(0))
                expect(LocationController.shared.getAll().count).notTo(equal(0))
                expect(ProfessionController.shared.getAll().count).notTo(equal(0))
                expect(ServiceController.shared.getAll().count).notTo(equal(0))
                expect(TreatmentController.shared.getAll().count).notTo(equal(0))
                expect(StateController.shared.getAll().count).notTo(equal(0))
                expect(CityController.shared.getAll().count).notTo(equal(0))
                expect(DistrictController.shared.getAll().count).notTo(equal(0))
            }
            
            it("get localization") {
                sdk.getLocalization()
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.GetLocale), timeout: 30.0)
                expect(LocalizationController.shared.get()).notTo(beNil())
            }
            
            it("get current profile") {
                sdk.getProfile()
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.GetProfile), timeout: 30.0)
                expect(ProfileController.shared.get()).notTo(beNil())
            }
            
            it("get patient"){
                sdk.getPatient()
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.GetPatient), timeout: 30.0)
                expect(PatientController.shared.getAll().count).notTo(equal(0))
            }
            
            it("updating localization") {
                sdk.updateLocalization(region: "IDN", language: "en")
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.UpdateLocale),timeout: 30.0)
            }
            
            it("updating profile") {
                sdk.updateProfile(firstName: "robzimpulse", lastName: "godlike", phone: "81222542156", nationality: "100", phone_code: "62")
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.UpdateProfile),timeout: 30.0)
            }
            
            it("updating password") {
                sdk.changePassword(password: "kiasu123", confirmPassword: "kiasu123")
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.ChangePassword),timeout: 30.0)
            }
            
            it("create new patient") {
                sdk.addPatient(name: "patient baru", dob: "1993-07-27", gender: "male", relationship: "3", condition: "1", conditionDetail: "stroke", height: "170", weight: "60", nationality: "100")
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.AddPatient),timeout: 30.0)
            }
            
            it("update new patient") {
                if let patient  = PatientController.shared.getAll().first {
                    sdk.updatePatient(id: patient.id, name: "patient baru", dob: "1993-07-27", gender: "male", relationship: "3", condition: "1", conditionDetail: "stroke", height: "170", weight: "60", nationality: "100")
                    expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.UpdatePatient),timeout: 30.0)
                }
            }
            
            it("search available freelancer for short term order") {
                sdk.getShortTermFreelancer(location: "25924", profession: "4", service: nil, treatment: nil)
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.GetShortTermFreelancer),timeout: 30.0)
                expect(ShortTermFreelancerController.shared.getAll().count).notTo(equal(0))
                expect(ShortTermFreelancerSlotController.shared.getAll().count).notTo(equal(0))
            }
            
            it("get history short term") {
                sdk.getHistoryShortTerm()
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.GetHistoryShortTerm), timeout: 30.0)
                expect(ShortTermHistoryController.shared.getAll().count).notTo(equal(0))
            }
            
            it("create new long term order") {
                sdk.createLongTerm(patientId: "g935l691j1", stateId: "25823", cityId: "25924", professionId: "6", freelancerGender: "male", serviceId: "cupiditate", session: "15", startDate: "2017-04-13", jobDetails: nil, addressDetail: nil)
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.CreateLongTerm), timeout: 30.0)
            }
            
            it("get history long term") {
                sdk.getHistoryLongTerm()
                expect(mock.lastCode).toEventually(equal(MynurzCustomerRequestCode.GetHistoryLongTerm), timeout: 30.0)
                expect(LongTermHistoryController.shared.getAll().count).notTo(equal(0))
            }
        }
    }
}
