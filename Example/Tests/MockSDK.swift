//
//  MockSDK.swift
//  mynurzSDK
//
//  Created by Robyarta on 4/25/17.
//  Copyright © 2017 kronus. All rights reserved.
//

import SwiftyJSON
import MynurzSDK

class MockSDK: MynurzCustomerDelegate, MynurzFreelancerDelegate {
    
    var lastCode: MynurzCustomerRequestCode?
    var lastCodeB: MynurzFreelancerRequestCode?
    var lastErrorCode: MynurzErrorCode?
    var status: Bool? {
        didSet{
            self.status = nil
        }
    }
    var data: JSON? {
        didSet{
            self.data = nil
        }
    }
    
    func responseError(message: String, code: MynurzCustomerRequestCode, errorCode: MynurzErrorCode, data: JSON?) {
        self.lastCode = code
        self.lastErrorCode = errorCode
        self.status = false
        self.data = data
        print("response error : \(message) - \(code)")
    }
    
    func responseSuccess(message: String, code: MynurzCustomerRequestCode, data: JSON) {
        self.lastCode = code
        self.lastErrorCode = nil
        self.status = true
        self.data = data
        print("response success : \(message) - \(code)")
    }
    
    func responseSuccess(message: String, code: MynurzFreelancerRequestCode, data: JSON) {
        self.lastCodeB = code
        self.lastErrorCode = nil
        self.status = true
        self.data = data
        print("response success : \(message) - \(code)")
    }
    
    func responseError(message: String, code: MynurzFreelancerRequestCode, errorCode: MynurzErrorCode, data: JSON?) {
        self.lastCodeB = code
        self.lastErrorCode = nil
        self.status = true
        self.data = data
        print("response success : \(message) - \(code)")
    }
}
