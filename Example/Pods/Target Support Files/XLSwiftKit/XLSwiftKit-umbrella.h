#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "UINavigationBar+Swift.h"
#import "XLFoundationSwiftKit-Bridgin-Header.h"
#import "XLFoundationSwiftKit.h"

FOUNDATION_EXPORT double XLSwiftKitVersionNumber;
FOUNDATION_EXPORT const unsigned char XLSwiftKitVersionString[];

